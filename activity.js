// a. what directive is used by Node.js in loading the module it needs
ans: require()
// b. what Node.js module contains a method for server creation 
ans: http
// c. what is the method of the http object responsible for creating a server using Node.js
ans: createServer()
// d. what method of the response object allows us to set status codes and content types
ans: writeHead()
// e. where will console.log() output its contents when run in Node.js
ans: CLI
// f. what property of the request object contains the address endpoint?
ans: url