const http = require('http')
const port = 3000
http.createServer((request,response) =>{
    console.log(`server is successfully running`)
    if(request.url === "/login"){
        response.writeHead(200, {"Content-type": "text/plain"})
        response.write("Welcome to the login page.")
        response.end()
    }
    else{
        response.writeHead(404, {"Content-type": "text/plain"})
        response.write("I'm sorry the page you are looking for cannot be found.")
        response.end()
    }
}).listen(port)